// vim: ts=4 sts=4 sw=4 expandtab

#ifndef EXPORTER_H
#define EXPORTER_H

#include "efg/efgtypes.h"

namespace Exporter {
    auto isDirOk(const QString &path) -> bool;
    auto write(const EFGDataSharedPtr &data, const QString &path, const QChar &fieldSeparator, const bool decimalComma) -> bool;
}

#endif // EXPORTER_H
