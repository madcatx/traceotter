// vim: ts=4 sts=4 sw=4 expandtab

#include "exporter.h"

#include <QFile>
#include <QFileInfo>
#include <QLocale>
#include <QTextStream>

static
auto colCaption(const QString &name, const QString &unit) -> QString
{
    QString ret = name;
    if (!unit.isEmpty())
        ret += QString{" [%1]"}.arg(unit);

    return QString{"\"%1\""}.arg(ret);
}

namespace Exporter {

auto isDirOk(const QString &path) -> bool
{
    QFileInfo fi{path};

    return fi.isDir() && fi.isWritable();
}

auto write(const EFGDataSharedPtr &data, const QString &path, const QChar &fieldDelimiter, const bool decimalComma) -> bool
{
    QFile fh{path};
    if (!fh.open(QIODevice::WriteOnly))
        return false;

    QLocale loc{decimalComma ? QLocale::German : QLocale::English};

    QTextStream stm{&fh};
    stm.setCodec("UTF-8");

    stm << colCaption(data->xType, data->xUnit) << fieldDelimiter << colCaption(data->yType, data->yUnit) << "\n";

    for (const auto &pt : data->data)
        stm << loc.toString(pt.x()) << fieldDelimiter << loc.toString(pt.y()) << "\n";

    return true;
}

}
