// vim: ts=4 sts=4 sw=4 expandtab

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class MainWindow : public QMainWindow {
public:
    explicit MainWindow();

private:
    QString m_exportPath;
    QChar m_fieldSeparator;
    bool m_decimalComma;
    QString m_prefix;
};

#endif // MAINWINDOW_H
