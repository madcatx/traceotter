// vim: ts=4 sts=4 sw=4 expandtab

#include "filelist.h"
#include "../efg/efgloaderinterface.h"

#include <QHBoxLayout>
#include <QListView>
#include <QMenu>
#include <QMessageBox>
#include <QPushButton>
#include <QStandardItemModel>
#include <QVBoxLayout>

#include <cassert>

#define EFG_DATA (Qt::UserRole + 1)
#define EFG_HASH (Qt::UserRole + 2)
#define EFG_NAME (Qt::UserRole + 3)

FileList::FileList(QWidget *parent)
    : QWidget{parent}
{
    auto buttonsLayout = new QHBoxLayout{};
    auto addFiles = new QPushButton{"Add files", this};
    auto removeFiles = new QPushButton{"Remove files", this};
    buttonsLayout->addWidget(addFiles);
    buttonsLayout->addWidget(removeFiles);

    auto addFilesMenu = new QMenu{this};
    addFiles->setMenu(addFilesMenu);

    auto topLayout = new QVBoxLayout{};
    auto list = new QListView{this};
    list->setSelectionMode(QListView::ExtendedSelection);

    m_listModel = new QStandardItemModel{0, 0, this};
    list->setModel(m_listModel);
    topLayout->addWidget(list);
    topLayout->addLayout(buttonsLayout);

    setLayout(topLayout);

    connect(removeFiles, &QPushButton::clicked,
            [this, list]() {
                auto sel = list->selectionModel()->selectedIndexes();
                std::sort(sel.begin(), sel.end());
                while (!sel.isEmpty()) {
                    m_listModel->removeRow(sel.constLast().row());
                    sel.pop_back();
                }
            }
    );
    connect(&EFGLoaderInterface::instance(), &EFGLoaderInterface::supportedFileFormatsRetrieved,
            [this, addFilesMenu](auto formats) {
                for (const auto &fmt : formats) {
                    auto tag = fmt.formatTag;
                    if (fmt.loadOptions.isEmpty()) {
                        auto a = new QAction{fmt.shortDescription, this};
                        connect(a, &QAction::triggered,
                                [this, tag]() {
                                    EFGLoaderInterface::instance().loadData(tag, 0);
                                }
                        );
                        addFilesMenu->addAction(a);
                    } else {
                        auto subMenu = new QMenu{fmt.shortDescription, this};
                        for (const auto &key : fmt.loadOptions.keys()) {
                            auto val = fmt.loadOptions[key];
                            auto a = new QAction{val, this};
                            connect(a, &QAction::triggered,
                                    [this, tag, key, val]() {
                                        EFGLoaderInterface::instance().loadData(tag, key);
                                    }
                            );
                            subMenu->addAction(a);
                        }
                        addFilesMenu->addMenu(subMenu);
                    }
                }
            }
    );
    connect(&EFGLoaderInterface::instance(), &EFGLoaderInterface::onDataLoaded,
            [this](EFGDataSharedPtr data, const DataHash &hash, const QString &path, const QString &name, const QString &id) {
                for (int row = 0; row < m_listModel->rowCount(); row++) {
                    const auto &data = m_listModel->data(m_listModel->index(row, 0), EFG_HASH);
                    assert(data.canConvert<DataHash>());
                    const auto chkHash = data.value<DataHash>();

                    if (hash == chkHash) {
                        QMessageBox::information(this, tr("Duplicit data"), tr("This data appears to have been already loaded"));
                        return;
                    }
                }

                auto item = new QStandardItem{path};
                item->setData(QVariant::fromValue(data), EFG_DATA);
                item->setData(QVariant::fromValue(hash), EFG_HASH);
                item->setData(name, EFG_NAME);
                item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
                m_listModel->appendRow(item);
            }
    );

    EFGLoaderInterface::instance().retrieveSupportedFileFormats();
}

auto FileList::dataAt(const int idx) const -> Data
{
    assert(idx >= 0 && idx < m_listModel->rowCount());

    auto index = m_listModel->index(idx, 0);
    auto v = m_listModel->data(index, EFG_DATA);
    auto name = m_listModel->data(index, EFG_NAME).toString();

    assert(v.canConvert<EFGDataSharedPtr>());

    return { v.value<EFGDataSharedPtr>(), std::move(name) };
}

auto FileList::numFiles() const -> int
{
    return m_listModel->rowCount();
}
