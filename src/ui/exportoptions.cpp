// vim: ts=4 sts=4 sw=4 expandtab

#include "exportoptions.h"

#include <QComboBox>
#include <QFileDialog>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QStandardItem>

ExportOptions::ExportOptions(QWidget *parent) :
    QWidget{parent}
{
    auto lay = new QFormLayout{};
    auto pathLay = new QHBoxLayout{};

    auto outPath = new QLineEdit{this};
    auto pathSel = new QFileDialog{this};
    auto browse = new QPushButton{tr("Browse"), this};
    pathSel->setAcceptMode(QFileDialog::AcceptOpen);
    pathSel->setFileMode(QFileDialog::Directory);
    pathSel->setOption(QFileDialog::ShowDirsOnly, true);

    pathLay->addWidget(outPath);
    pathLay->addWidget(browse);

    auto fieldSeparator = new QComboBox{this};
    fieldSeparator->addItem("Semicolon (;)", QVariant{';'});
    fieldSeparator->addItem("Comma (,)", QVariant{','});
    fieldSeparator->addItem("Space", QVariant{' '});
    fieldSeparator->addItem("Tab", QVariant{'\t'});
    fieldSeparator->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

    auto decimalSeparator = new QComboBox{this};
    decimalSeparator->addItem("Period (.)", '.');
    decimalSeparator->addItem("Comma (,)", ',');
    decimalSeparator->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);

    auto prefix = new QLineEdit{this};

    auto reporter = [this, outPath, fieldSeparator, decimalSeparator, prefix]() {
        auto path = outPath->text();
        auto sep = fieldSeparator->currentData().toChar();
        auto comma = decimalSeparator->currentData().toChar() == ',';
        auto pfx = prefix->text();

        emit changed(path, sep, comma, pfx);
    };

    connect(browse, &QPushButton::clicked,
            [this, outPath, pathSel]() {
                pathSel->setDirectory(QDir{outPath->text()}.absolutePath());
                if (pathSel->exec() == QDialog::Accepted)
                    outPath->setText(pathSel->selectedFiles().constFirst());
            }
    );

    connect(outPath, &QLineEdit::textChanged, reporter);
    connect(fieldSeparator, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), reporter);
    connect(decimalSeparator, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), reporter);
    connect(prefix, &QLineEdit::textChanged, reporter);

    lay->addRow(new QLabel{tr("Export path"), this}, pathLay);
    lay->addRow(new QLabel{tr("Field separator"), this}, fieldSeparator);
    lay->addRow(new QLabel{tr("Decimal separator"), this}, decimalSeparator);
    lay->addRow(new QLabel{tr("Prefix")}, prefix);

    setLayout(lay);
}
