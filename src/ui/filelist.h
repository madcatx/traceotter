// vim: ts=4 sts=4 sw=4 expandtab

#ifndef FILELIST_H
#define FILELIST_H

#include "../efg/efgtypes.h"

#include <QWidget>

class QStandardItemModel;

class FileList : public QWidget {
public:
    class Data {
    public:
        EFGDataSharedPtr data;
        QString name;
    };

    explicit FileList(QWidget *parent);

    auto dataAt(const int idx) const -> Data;
    auto numFiles() const -> int;

private:
    QStandardItemModel *m_listModel;
};

#endif // FILELIST_H
