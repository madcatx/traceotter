// vim: ts=4 sts=4 sw=4 expandtab

#ifndef EXPORTOPTIONS_H
#define EXPORTOPTIONS_H

#include <QWidget>

class ExportOptions : public QWidget {
    Q_OBJECT
public:
    explicit ExportOptions(QWidget *parent = nullptr);

signals:
    void changed(const QString &path, const QChar &fieldDelimiter, const bool decimalComma, const QString &prefix);
};

#endif // EXPORTOPTIONS_H
