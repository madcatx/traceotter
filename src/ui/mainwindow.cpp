// vim: set ts=4 sts=4 sw=4 expandtab

#include "mainwindow.h"
#include "exportoptions.h"
#include "../exporter.h"
#include "filelist.h"

#include <QHBoxLayout>
#include <QMessageBox>
#include <QPushButton>

MainWindow::MainWindow() :
    QMainWindow{nullptr}
{
    auto cw = new QWidget{this};
    auto lay = new QVBoxLayout{this};
    auto ctrlsLay = new QHBoxLayout{};
    auto fileList = new FileList{this};
    auto exportOptions = new ExportOptions{this};

    ctrlsLay->addWidget(fileList, 3);
    ctrlsLay->addWidget(exportOptions, 2);

    auto doExport = new QPushButton{"Export", this};

    lay->addLayout(ctrlsLay);
    lay->addWidget(doExport);

    cw->setLayout(lay);

    setCentralWidget(cw);

    connect(exportOptions, &ExportOptions::changed,
            [this](const QString &path, const QChar &sep, const bool comma, const QString &prefix) {
                m_exportPath = path;
                m_fieldSeparator = sep;
                m_decimalComma = comma;
                m_prefix = prefix;
            }
    );

    connect(doExport, &QPushButton::clicked,
            [this, fileList]() {
                if (!Exporter::isDirOk(m_exportPath)) {
                    QMessageBox::warning(this, tr("IO error"), tr("Cannot write to the selected directory"));
                    return;
                }

                for (int idx = 0; idx < fileList->numFiles(); idx++) {
                    const auto data = fileList->dataAt(idx);

                    auto name = data.name;
                    auto dotIdx = name.lastIndexOf('.');
                    if (dotIdx > 1) {
                        name = name.mid(0, dotIdx + 1);
                        name += "csv";
                    }

                    const auto path = m_exportPath + "/" + m_prefix + name;

                    if (!Exporter::write(data.data, path, m_fieldSeparator, m_decimalComma))
                        QMessageBox::warning(this, tr("IO error"), QString{tr("Failed to write file %1")}.arg(name));
                }

                QMessageBox::information(this, tr("Success!"), tr("Done..."));
            }
    );
}
