// vim: ts=4 sts=4 sw=4 expandtab

#include "globals.h"
#include "efg/efgloaderinterface.h"
#include "efg/efgloaderwatcher.h"
#include "ui/mainwindow.h"

#include <QApplication>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QDir>
#include <QMessageBox>
#include <cstdlib>

#define EDII_DIR "EDII"

static
auto checkEDIIServicePath(const QDir &path) -> bool
{
    if (!efg::EFGLoaderWatcher::isServicePathValid(path.absolutePath())) {
        QMessageBox::critical(
                nullptr,
                QObject::tr("No path to EDII service"),
                QObject::tr("Path to EDII serivce is invalid. Check that there is a subdirectory named \"" EDII_DIR "\" in " SOFTWARE_NAME_INTERNAL_S " directory")
        );
        return false;
    }

    return true;
}

auto main(int argc, char * argv[]) -> int
{
    QApplication app{argc, argv};
    QApplication::setApplicationName(Globals::SOFTWARE_NAME);

#ifdef TRACEOTTER_FLATPAK_BUILD
    QString ediiServicePath = "/app";
#else
    auto ediiServicePath = QDir::current();

    QCommandLineOption ediiPathOpt{"edii", "Path to EDII installation", "ediiPath"};
    QCommandLineParser cmdParser{};
    cmdParser.setApplicationDescription(QObject::tr("Chromatographic signal trace converter"));
    cmdParser.addHelpOption();
    cmdParser.addOption(ediiPathOpt);

    cmdParser.process(app);
    if (cmdParser.isSet(ediiPathOpt)) {
        const auto val = cmdParser.value(ediiPathOpt);
        if (val.isEmpty()) {
            QMessageBox::critical(nullptr, QObject::tr("Invalid options"), QObject::tr("Invalid value of EDII path parameter"));
            return EXIT_FAILURE;
        }

        ediiServicePath = QDir{val};
    }

    ediiServicePath.cd(EDII_DIR);
#endif // TRACEOTTER_FLATPAK_BUILD

    if (!checkEDIIServicePath(ediiServicePath))
        return EXIT_FAILURE;

    try {
        EFGLoaderInterface::initialize(ediiServicePath.absolutePath());
    } catch (const std::runtime_error &ex) {
        QMessageBox::critical(
            nullptr,
            QObject::tr("Cannot connect to EDII service"),
            QString{QObject::tr("Cannot connect to EDII service: %1")}.arg(ex.what())
        );

        return EXIT_FAILURE;
    }

    MainWindow mWin{};
    mWin.show();

    return app.exec();
}
