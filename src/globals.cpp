#include "globals.h"

#include <QIcon>

const QString Globals::ORG_DOMAIN("echmet.natur.cuni.cz");
const QString Globals::ORG_NAME("ECHMET");
const QString Globals::SOFTWARE_NAME("TraceOtter");
const QString Globals::SOFTWARE_NAME_INTERNAL(SOFTWARE_NAME_INTERNAL_S);


QIcon Globals::ICON()
{
#ifdef Q_OS_WIN
  static const QPixmap PROGRAM_ICON(":/images/res/TraceOtter_icon.ico");
#else
  static const QPixmap PROGRAM_ICON(":/images/res/TraceOtter_icon_64.png");
#endif // Q_OS_WIN

  if (PROGRAM_ICON.isNull())
      return {};

  return QIcon(PROGRAM_ICON);
}

QString Globals::VERSION_STRING()
{
    return "TraceOtter";
}
