#ifndef GLOBALS_H
#define GLOBALS_H

#include <QString>
#include <QVector>

#define SOFTWARE_NAME_INTERNAL_S "TraceOtter"

class QIcon;

class Globals {
public:
  Globals() = delete;

  static QIcon ICON();
  static QString VERSION_STRING();

  static const QString ECHMET_WEB_LINK;
  static const QString ORG_DOMAIN;
  static const QString ORG_NAME;
  static const QString SOFTWARE_NAME;
  static const QString SOFTWARE_NAME_INTERNAL;

};

#endif // GLOBALS_H
